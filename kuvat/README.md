# Kuvat

Kansio sisältää raportissa käytetyt kuvat. Kuviin täytyy viitata tiedostosta
`main.tex` käsin muodossa

	kuvat/kuva.pääte ,

missä `pääte` ∈ {`pdf`,`eps`,`png`,`jpg`}. Näistä LaTeΧin kannalta paras on
`pdf`.
