# TAU Fysiikan oppilaslaboratorion selostuspohja

Tämä repositorio sisältää selostuspohjan Tampereen yliopiston fysiikan
oppilaslaboratorion raportteja varten. Saat ladattua tiedostot omalle
koneellesi painamalla tämän sivun ylälaidasta <kbd>Download</kbd>/<kbd>⌊↓⌋</kbd> →
<kbd>zip</kbd> ja tallentamalla tiedoston haluamaasi kansioon.

Pohja on kirjoitettu tieteellisen tekstin kirjoittamiseen suunnitellulla
ladontajärjestelmällä [LaTeΧ]. [Overleaf] tarjoaa lyhyehkön
[LaTeΧ-oppaan][Overleaf-LaTeX-opas], jossa käydään läpi kielen perusteet, jos
sen käyttö ei vielä ole tuttua. Siinä, missä ohjelmat kuten MS Word ja
LibreOffice Writer noudattavat [WYSIWYG]-periaatetta, on LaTeΧ
[WYSIWYM]-periaatetta noudattava järjestelmä.

[LaTeΧ]: https://www.latex-project.org/
[Overleaf]: https://www.overleaf.com/
[Overleaf-LaTeX-opas]: https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes
[WYSIWYG]: https://fi.wikipedia.org/wiki/WYSIWYG
[WYSIWYM]: https://fi.wikipedia.org/wiki/WYSIWYM

## Sisällysluettelo

* [Käyttö palvelussa Overleaf](#käyttö-palvelussa-overleaf-suositeltavaa)
* [Muokattavat tiedostot ja kansiot](#muokattavat-tiedostot-ja-kansiot)
* [Paikallinen käyttö](#paikallinen-käyttö)
* [Ennalta määriteltyjä komentoja ja ympäristöjä](#ennalta-määriteltyjä-komentoja-ja-ympäristöjä)
* [Lisenssi](#lisenssi)


## Käyttö palvelussa Overleaf (suositeltavaa)

[Overleaf] on palvelu, joka mahdollistaa LaTeΧ-dokumenttien kirjoittamisen
yhteistyössä muiden kanssa verkossa, samaan tapaan kuin Google Docs, tai
Microsoftin Office 365 tekevät kyseisten toimijoiden omien
kirjoitusformaattien tapauksessa. Saat kirjoituspohjan käyttöön Overleafissa
seuraavien ohjeiden mukaisesti.

Lataa tämän repositorion sisältö ZIP-tiedostona sivun ylälaidan
<kbd>Download</kbd>-painikkeesta ja nimeä ladattu ZIP-tiedosto työn nimen
mukaan. [Luo tili][Overleaf-tili] Overleafiin tai [kirjaudu
palveluun][Overleaf-kirjautuminen], jos sinulla on jo Overleaf-tili käytössä.

Kun olet kirjautunut Overleafiin, voit luoda uuden projektin lataamalla täältä
lataamasi ZIP-paketin palveluun. Tämä tapahtuu painamalla <kbd>New
Project</kbd> → <kbd>Upload Project</kbd> ja valitsemalla lataamasi
ZIP-tiedosto. Overleaf lataa tiedoston ja luo projektin sen perusteella.
Vaihda vielä lopuksi projektin oletuskääntäjää painamalla projektin sivulla
<kbd>Menu</kbd> → <kbd>Compiler</kbd> → <kbd>LuaLaTeX</kbd>. Tämän jälkeen
voit muokata tiedoston `main.tex` tekstiä Overleafin tekstieditorissa. Älä
myöskään unohda täydentää dokumentin tietoja tiedostoon `docinfo.tex`.

Voit ladata Overleafin tuottaman PDF-tiedoston painamalla projektin sivulla
joko vasemman palkin <kbd>PDF</kbd>-painiketta, tai sitten oikealla olevan
PDF-näkymän yläpuolelta <kbd>Download PDF</kbd>. Ladatun PDF-tiedoston voi
sitten palauttaa [Alvaan][Alva].

[Overleaf-tili]: https://www.overleaf.com/register
[Overleaf-kirjautuminen]: https://www.overleaf.com/login


## Muokattavat tiedostot ja kansiot

Tässä on lyhyt kuvaus tiedostoista, joita pohjan käyttäjän on tarkoitus
muokata. Lisenssitiedostoa `LICENSE` lukuunottamatta muihinkin tiedostoihin
saa koskea lisenssin asettamien ehtojen puitteissa, jos tietää mitä tekee.

### [docinfo.tex](./docinfo.tex)

Sisältää itse työn ja kirjoittajien tiedot määrittelyjen

	\def\avain{arvo}

avulla ilmoitettuna. Täydennä työn tiedot tänne ennen sen palauttamista
[Alvaan][Alva]. Avaimien `\avain` nimien pitäisi olla sen verran kuvaavia,
että niistä pystyy päättelemään mitä kunkin määrittelyn tulee sisältää
arvonaan.

[Alva]: https://alva.tuni.fi/

### [main.tex](./main.tex)

Tarkoitus on, että itse raportti kirjoitetaan tähän tiedostoon. Tiedosto
sisältää valmiiksi tekstiä, jonka on tarkoitus toimia esimerkkinä siitä miten
LaTeΧia kirjoitetaan.

### [lahteet.bib](./lahteet.bib)

Lähteiden tiedot lisätään tänne. Tiedosto on kirjoitettu lähteiden
ladontaohjelman [BibLaTeΧ][BibLaTeΧ file] tunnistamalla syntaksilla ja
sisältää valmiita esimerkkejä verkko- ja kirjallisuuslähteiden, vastaavasti
`@online` ja `@book`, listaamiseen. Hyvän arvosanan saamiseksi tarvitaan
todennäköisesti myös muita lähteitä kuin mitä tiedostossa on valmiiksi
listattu…

Kaikkia tässä tiedostossa olevia lähteitä ei automaattisesti sisällytetä
raportin sisällysluetteloon, vaan ainoastaan ne joihin oikeasti viitataan
komennolla

	\cite{lähteen avain}

jossakin päin tekstiä päätyvät sinne. Lähteen avain on jokaisen
lähdemäärittelyn ensimmäinen avaimeton kenttä:

	@lähteen tyyppi {
		lähteen avain,
		kenttä 1 = {arvo},
		⋮
		kenttä 𝑁 = {arvo}
	}

Eri lähteillä on erilaisia tarvittavia kenttiä. Tarkemmat tiedot löytyvät
[BibLaTeΧin dokumentaatiosta][BibLaTeΧ doc], luvusta 2.1.

[BibLaTeΧ file]: https://www.overleaf.com/learn/latex/Bibliography_management_with_biblatex#The_bibliography_file
[BibLaTeΧ doc]: https://www.texlive.info/CTAN/macros/latex/contrib/biblatex/doc/biblatex.pdf

### [kuvat/](./kuvat)

Tämä kansio on yksinkertaisesti raportin kuvien tallentamista varten. Näihin
sitten viitataan tiedostosta `main.tex` käsin muodossa
`kuvat/kuvan_tiedostonimi`.

### [preamble.tex](./preamble.tex)

Tähän tiedostoon voi tehdä omia LaTeΧ-komentojen tai -ympäristöjen
määrityksiä, mutta tähän ei välttämättä ole tarvetta, sillä perus-LaTeΧilla
pärjää melko pitkälle. Käytössä on oletuksena kirjasto [xparse], joka
mahdollistaa uusien komentojen määrittelemisen robustimmalla syntaksilla kuin
LaTeΧin itse tarjoama makro `\newcommand`. Esimerkiksi komennolle `\mathrm` on
mahdollista määritellä lyhenne

	\NewDocumentCommand\mrm{m}{\mathrm{#1}}

joka mahdollistaa pystyfontilla kirjoittamisen matematiikkatilassa:
`\(\mrm{tekstiä}\)`. Tässä siis `mrm` on komennon nimi, `{m}` sanoo että
komento ottaa yhden pakollisen (mandatory) argumentin ja `{\mathrm{#1}}` sanoo
että tämä ensimmäinen argumentti `#1` tulee laittaa komennon `\mathrm`
sisälle. Lisätietoja syntaksista löytyy mm. [kirjaston xparse
dokumentaatiosta][xparse-doc]

[xparse]: https://ctan.org/pkg/xparse
[xparse-doc]: https://www.nic.funet.fi/pub/TeX/CTAN/macros/latex/contrib/l3packages/xparse.pdf


## Paikallinen käyttö

Tarvitset tekstinmuokkausohjelman kuten [VS Codium], LaTeΧ-jakelun kuten [TeX
Live] sekä jonkinlaisen komentotulkin ([fish]|[Zsh]|[Bash]|[Windows
Terminal]|[PowerShell]). Kun nämä on asennettu, pura täältä lataamasi
ZIP-tiedoston sisältö omaan kansioonsa, vaikkapa komennolla
(komentoriviriippuvainen)

	unzip \
		polku/zipin/tallennuskansioon/tau-fysiikan-oppilaslaboration-selostuspohja-main.zip \
		-d $HOME

Navigoi komentorivillä tähän purettuun kansioon komennolla

	cd $HOME/tau-fysiikan-oppilaslaboration-selostuspohja-main

Kunhan komento `pwd` tulostaa nyt tiedoston `main.tex` sisältävän kansion
polun, voit ajaa komentorivillä komennot

	lualatex main.tex
	biber main
	lualatex main.tex

tuottaaksesi tiedostoon `main.tex` kirjoittamastasi tekstistä PDF-tiedoston,
jonka voit palauttaa [Alvaan][Alva]. Tässä ohjelma `lualatex` on
LaTeΧ-kääntäjä, joka tuottaa kirjoittamastasi LaTeΧ-koodista PDF-tiedoston.
Ohjelma `biber` taas on vastuussa raportin lähdeluettelon luonnista.

[VS Codium]: https://vscodium.com/
[TeX Live]: https://www.tug.org/texlive/
[fish]: https://fishshell.com/
[Zsh]: https://www.zsh.org/
[Bash]: https://www.gnu.org/software/bash/
[Windows Terminal]: https://docs.microsoft.com/en-us/windows/terminal/install
[PowerShell]: https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell?view=powershell-7.2

## Ennalta määriteltyjä komentoja ja ympäristöjä

Kirjoituspohjan luokkatiedosto sisältää muutamia ennalta määriteltyjä
hyödyllisiä komentoja ja ympäristöjä. *Parilliset rajoitinkomennot*

	\kaaret{sisältö}
	\joukko{sisältö}
	\aallot{sisältö}
	\haat{sisältö}
	\itseis{sisältö}

toimivat kaikki samaan tapaan: ne ympäröivät `sisällön` erinäisillä
parillisilla rajoittimilla. Esimerikisi `\haat{sisältö}` tuottaa lopullisessa
PDF-tiedostossa tulosteekseen `[sisältö]`. Rajoittimia voi *skaalata*
sisältönsä mukaan sijoittamalla merkin `*` ennen komennon aaltosukkeita.
Esimerkiksi

	\kaaret*{sisältö}

kasvattaa ympäröiviä merkkejä `(` ja `)` siten, että ne ovat yhtä korkeita
kuin `sisältö`. Tämä on erityisen hyödyllistä korkeiden murtolausekkeiden
kirjoittamisen tapauksessa.

Kuvat ja taulukot sijoitetaan selostuksiin ympäristöillä `kuva` ja `taulukko`.
Kuvien syntaksi on seuraavan mukainen:

	\begin{kuva}[vaihtoehtoinen leveysasetus]{kuvateksti}{viittausnimi}
		\includegrapics[width=\linewidth]{polku/kuvatiedostoon.pdf}
	\end{kuva}

Taulukot taas sijoitellaan selostukseen syntaksilla

	\begin{taulukko}[vaihtoehtoinen leveysasetus]{taulukkoteksti}{viittausnimi}
		\begin{tabular}{𝑛 sarakkeen tasausmäärittelyt}
			solu (1,1) & solu (1,2) & ⋯ & solu (1,𝑛) \\
			solu (2,1) & solu (2,2) & ⋯ & solu (2,𝑛) \\
			⋮
			solu (𝑚,1) & solu (𝑚,2) & ⋯ & solu (𝑚,𝑛) \\
		\end{tabular}
	\end{taulukko}

missä ympäristön `tabular` sarakkeiden tasausmääreet voivat olla joko `l`, `c`
tai `r`, vastaavasti *vasen*, *keskitetty* ja *oikea*. Jos kokonainen sarake
täytyy kirjoittaa matematiikkatilassa, käytä määreitä `L`, `C` tai `R` pienten
kirjaimien sijaan.

Kuvia ja taulukoita voi sijoittaa vierekkäin jättämällä tyhjät rivit pois
niiden välistä:

	\begin{kuva}[0.45\linewidth]{Kuvateksti vasemmalle kuvalle.}{fig:vasen-kuva}
		\includegrapics[width=\linewidth]{polku/vasemmanpuoleiseen/kuvaan.pdf}
	\end{kuva}
	\hfill % ← Työntää kuvat sivun reunaan
	\begin{kuva}[0.45\linewidth]{Kuvateksti oikealle kuvalle.}{fig:oikea-kuva}
		\includegrapics[width=\linewidth]{polku/oikeanpuoleiseen/kuvaan.pdf}
	\end{kuva}

Jos näin asetellut kuvat osoittautuvat pienikokoisiksi ja näin
vaikealukuisiksi, kannattaa vierekkäin sijoittelua välttää.

Luokkatiedostossa saattaa olla määriteltyinä myös muita komentoja kuten `\pd`,
joka on lyhennelmä komennosta `\partial` ja tuottaa tulosteekseen symbolin ∂.
Nämä ovat kuitenkin vähemmän oleellisia, ja tässä ohjeessa mainitut
rajoitinkomennot ja ympäristöt ovat oleellisempia tiedostaa.


## Lisenssi

### TAU Fysiikan oppilaslaboratorion selostuspohja

Copyright © 2022 Santtu Söderholm

This work may be distributed and/or modified under the conditions of the LaTeX
Project Public License, either version 1.3 of this license or (at your option)
any later version. The latest version of this license is in
<http://www.latex-project.org/lppl.txt> and version 1.3 or later is part of
all distributions of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status "maintained". The Current Maintainer
of this work is Santtu Söderholm (<santtu.soderholm@tuni.fi>).

This work consists of the files in the folders `tex/` and
`kuvat/mittaustulokset/`.
